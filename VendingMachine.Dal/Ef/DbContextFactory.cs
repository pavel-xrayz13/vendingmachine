﻿using System.Threading;
using VendingMachine.Domain;

namespace VendingMachine.Dal.Ef
{
    public class DbContextFactory : IDbContextFactory
    {
        private readonly ThreadLocal<VendingMachineDbContext> _localUnitOfWork;

        public DbContextFactory(string connectionString)
        {
            _localUnitOfWork = new ThreadLocal<VendingMachineDbContext>(() => 
                new VendingMachineDbContext(connectionString));
        }

        public IDbContext Get(bool commitOnDispose = true)
        {
            return new DbContext(_localUnitOfWork.Value, commitOnDispose);
        }
    }
}
