﻿using System.Data.Entity;
using VendingMachine.Dal.Ef.Initializers;

namespace VendingMachine.Dal.Ef
{
    internal class VendingMachineDbContext : System.Data.Entity.DbContext
    {
        public VendingMachineDbContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer(new VendingMachineDefaultInitializer());
        }
    }
}
