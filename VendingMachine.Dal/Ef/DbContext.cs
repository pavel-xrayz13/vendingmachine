﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using VendingMachine.Domain;

namespace VendingMachine.Dal.Ef
{
    internal class DbContext : IDbContext
    {
        private VendingMachineDbContext _dbContext;
        private readonly bool _commitOnDispose;

        public DbContext(VendingMachineDbContext dbContext, bool commitOnDispose)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _commitOnDispose = commitOnDispose;
        }

        public void AddEntity<TEntity>(TEntity entity) where TEntity : class
        {
            _dbContext.Set<TEntity>().Add(entity);
        }

        public void UpdateEntity<TEntity>(TEntity entity) where TEntity : class
        {
            _dbContext.Set<TEntity>().Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void RemoveEntity<TEntity>(TEntity entity) where TEntity : class
        {
            var set = _dbContext.Set<TEntity>();
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                set.Attach(entity);
            }

            set.Remove(entity);
        }

        public TEntity FindEntity<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            return _dbContext.Set<TEntity>().FirstOrDefault(filter);
        }

        public IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null) where TEntity : class
        {
            IQueryable<TEntity> query = _dbContext.Set<TEntity>();
            if (filter != null)
            {
                query = query.Where(filter);
            }

            return orderBy != null ? orderBy(query) : query;
        }

        public void Commit()
        {
            try
            {
                if (_dbContext.ChangeTracker.HasChanges())
                {
                    _dbContext.SaveChanges();
                }
            }
            catch
            {
                Rollback();
                throw;
            }
        }

        private void Rollback()
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

        public void Dispose()
        {
            if (_commitOnDispose)
            {
                Commit();
            }

            _dbContext = null;
        }
    }
}
