﻿using System.Collections.Generic;
using System.Data.Entity;
using VendingMachine.Domain.Entities;

namespace VendingMachine.Dal.Ef.Initializers
{
    internal class VendingMachineDefaultInitializer : DropCreateDatabaseIfModelChanges<VendingMachineDbContext>
    {
        protected override void Seed(VendingMachineDbContext context)
        {
            var coins = new List<CoinsEntity>
            {
                new CoinsEntity { Nominal = 1, Quantity = 10 },
                new CoinsEntity { Nominal = 2, Quantity =  10 },
                new CoinsEntity { Nominal = 5, Quantity = 10 },
                new CoinsEntity { Nominal = 10, Quantity = 10 }
            };

            var drinks = new List<DrinkEntity>
            {
                new DrinkEntity { Name = "Soda", Price = 5, Quantity = 5 },
                new DrinkEntity { Name = "Coffee", Price = 3, Quantity = 10 },
                new DrinkEntity { Name = "Water", Price = 2, Quantity = 15 }
            };

            var coinsSet = context.Set<CoinsEntity>();
            var drinksSet = context.Set<DrinkEntity>();

            coins.ForEach(e => coinsSet.Add(e));
            drinks.ForEach(e => drinksSet.Add(e));

            context.SaveChanges();
        }
    }
}
