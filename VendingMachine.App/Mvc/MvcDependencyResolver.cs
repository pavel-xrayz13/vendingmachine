﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SimpleInjector;

namespace VendingMachine.App.Mvc
{
    public class MvcDependencyResolver : IDependencyResolver
    {
        public MvcDependencyResolver(Container container)
        {
            Container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public Container Container { get; }

        private IServiceProvider ServiceProvider => Container;

        public object GetService(Type serviceType)
        {
            if (!serviceType.IsAbstract && typeof(IController).IsAssignableFrom(serviceType))
            {
                return Container.GetInstance(serviceType);
            }

            return ServiceProvider.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            var collectionType = typeof(IEnumerable<>).MakeGenericType(serviceType);
            var services = (IEnumerable<object>)ServiceProvider.GetService(collectionType);
            return services ?? Enumerable.Empty<object>();
        }
    }
}