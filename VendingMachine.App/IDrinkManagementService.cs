﻿using System.Collections.Generic;
using System.Web;
using VendingMachine.App.Dtos;

namespace VendingMachine.App
{
    public interface IDrinkManagementService
    {
        void ModifyOrAddDrink(DrinkDto drink, HttpPostedFileBase image);
        IEnumerable<DrinkDto> AllAvailableDrinks();
    }
}
