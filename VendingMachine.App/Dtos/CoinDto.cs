﻿namespace VendingMachine.App.Dtos
{
    public class CoinDto
    {
        public int CoinId { get; set; }
        public int Nominal { get; set; }
        public int Quantity { get; set; }
        public bool IsBlocked { get; set; }
    }
}
