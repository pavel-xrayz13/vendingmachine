﻿namespace VendingMachine.App.Dtos
{
    public class DrinkDto
    {
        public int DrinkId { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public byte[] ImageData { get; set; }
    }
}
