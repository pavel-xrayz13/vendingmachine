﻿using System.Collections.Generic;
using VendingMachine.App.Dtos;

namespace VendingMachine.App
{
    public interface ICoinManagementService
    {
        void SetCoinQuantity(int coinType, int count);
        Dictionary<int, int> CalculateRestFrom(int money);
        IEnumerable<CoinDto> AllAvailableCoins();
    }
}
