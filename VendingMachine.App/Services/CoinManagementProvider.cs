﻿using System;
using System.Collections.Generic;
using System.Linq;
using VendingMachine.App.Dtos;
using VendingMachine.Domain;
using VendingMachine.Domain.Entities;

namespace VendingMachine.App.Services
{
    public class CoinManagementProvider : ICoinManagementService
    {
        private readonly IDbContextFactory _uowFactory;

        public CoinManagementProvider(IDbContextFactory uowFactory)
        {
            _uowFactory = uowFactory ?? throw new ArgumentNullException(nameof(uowFactory));
        }

        public void SetCoinQuantity(int coinType, int coinCount)
        {
            using (var uow = _uowFactory.Get())
            {
                var coin = uow.FindEntity<CoinsEntity>(e => e.Nominal == coinType);

                if (coin != null)
                {
                    coin.Quantity = coinCount;
                    uow.UpdateEntity(coin);
                }
                else
                {
                    coin = new CoinsEntity
                    {
                        Nominal = coinType,
                        Quantity = coinCount
                    };

                    uow.AddEntity(coin);
                }
            }
        }

        public Dictionary<int, int> CalculateRestFrom(int money)
        {
            var dictionary = new Dictionary<int, int>();
            int[] faceValues = { 10, 5, 2, 1 };

            foreach (var item in faceValues)
            {
                if (money / item == 0) continue;
                dictionary.Add(item, money / item);
                money %= item;
                if (money == 0) break;
            }

            return dictionary;
        }

        public IEnumerable<CoinDto> AllAvailableCoins()
        {
            using (var uow = _uowFactory.Get(commitOnDispose: false))
            {
                return uow.Query<CoinsEntity>(e => e.Quantity > 0).Select(e => new CoinDto
                {
                    CoinId = e.Id,
                    Nominal = e.Nominal,
                    Quantity = e.Quantity,
                    IsBlocked = false
                }).ToList();
            }
        }
    }
}
