﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VendingMachine.App.Dtos;
using VendingMachine.Domain;
using VendingMachine.Domain.Entities;

namespace VendingMachine.App.Services
{
    public class DrinkManagementProvider : IDrinkManagementService
    {
        private readonly IDbContextFactory _uowFactory;

        public DrinkManagementProvider(IDbContextFactory uowFactory)
        {
            _uowFactory = uowFactory ?? throw new ArgumentNullException(nameof(uowFactory));
        }

        public void ModifyOrAddDrink(DrinkDto drink, HttpPostedFileBase image)
        {
            void UpdateImage(DrinkEntity entity, HttpPostedFileBase httpPostedFile)
            {
                httpPostedFile?.InputStream.Read(entity.PreviewImage, 0, httpPostedFile.ContentLength);
            }

            using (var uow = _uowFactory.Get())
            {
                DrinkEntity oldDrinkEntity;
                if (drink.DrinkId != 0)
                {
                    oldDrinkEntity = uow.FindEntity<DrinkEntity>(e => e.Id == drink.DrinkId);
                    oldDrinkEntity.Name = drink.Name;
                    oldDrinkEntity.Quantity = drink.Quantity;
                    oldDrinkEntity.Price = drink.Price;
                    UpdateImage(oldDrinkEntity, image);

                    uow.UpdateEntity(oldDrinkEntity);
                }
                else
                {
                    oldDrinkEntity = new DrinkEntity
                    {
                        Name = drink.Name,
                        Price = drink.Price,
                        Quantity = drink.Quantity,
                        PreviewImage = drink.ImageData
                    };
                    UpdateImage(oldDrinkEntity, image);

                    uow.AddEntity(oldDrinkEntity);
                }
            }
        }

        public IEnumerable<DrinkDto> AllAvailableDrinks()
        {
            using (var uow = _uowFactory.Get(commitOnDispose: false))
            {
                return uow.Query<DrinkEntity>(e => e.Quantity > 0).Select(e => new Drink
                {
                    DrinkId = e.Id,
                    Quantity = e.Quantity,
                    Price = e.Price,
                    ImageData = e.PreviewImage
                }).ToList();
            }
        }
    }
}
