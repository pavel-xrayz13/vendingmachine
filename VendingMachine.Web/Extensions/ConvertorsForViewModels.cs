﻿using System;
using System.Collections.Generic;
using System.Linq;
using VendingMachine.App.Dtos;
using VendingMachine.Web.Models;

namespace VendingMachine.Web.Extensions
{
    public static class ConvertorsForViewModels
    {
        public static IEnumerable<CoinViewModel> AsCoinViewModels(this IEnumerable<CoinDto> self)
        {
            if(self == null)
                throw new ArgumentNullException(nameof(self));

            return self.Select(e => new CoinViewModel
            {
                CoinId = e.CoinId,
                Nominal = e.Nominal,
                Quantity = e.Quantity,
                IsBlocked = e.IsBlocked
            });
        }

        public static IEnumerable<DrinkViewModel> AsDrinkViewModels(this IEnumerable<DrinkDto> self)
        {
            if (self == null)
                throw new ArgumentNullException(nameof(self));

            return self.Select(e => new DrinkViewModel
            {
                DrinkId = e.DrinkId,
                Name = e.Name,
                Price = e.Price,
                Quantity = e.Quantity,
                ImageData = e.ImageData
            });
        }
    }
}