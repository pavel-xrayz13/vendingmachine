﻿using System;
using System.Web.Mvc;
using VendingMachine.App;
using VendingMachine.Web.Extensions;
using VendingMachine.Web.Models;

namespace VendingMachine.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICoinManagementService _coinManagementService;
        private readonly IDrinkManagementService _drinkManagementService;

        public HomeController(ICoinManagementService coinManagementService,
            IDrinkManagementService drinkManagementService)
        {
            _coinManagementService = coinManagementService ?? 
                throw new ArgumentNullException(nameof(coinManagementService));

            _drinkManagementService = drinkManagementService ?? 
                throw new ArgumentNullException(nameof(drinkManagementService));
        }

        [HttpGet]
        public ActionResult Index()
        {
            const string secretId = "secret";
            var redirectToAdmin = Request.QueryString["id"] == secretId;
            return redirectToAdmin ? RedirectToAdmin() : RedirectToIndex();
        }

        [NonAction]
        private ActionResult RedirectToIndex()
        {
            var drinks = _drinkManagementService.AllAvailableDrinks().AsDrinkViewModels();
            var coins = _coinManagementService.AllAvailableCoins().AsCoinViewModels();

            return View(new HomeModel
            {
                Drinks = drinks,
                Coins = coins
            });
        }

        [NonAction]
        private ActionResult RedirectToAdmin()
        {
            return Redirect(Url.Action("Index", "Admin"));
        }
    }
}