﻿using System.ComponentModel.DataAnnotations;

namespace VendingMachine.Web.Models
{
    public class DrinkViewModel
    {
        public int DrinkId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [Range(0, 200)]
        public int Price { get; set; }

        [Required]
        [Range(0, 10)]
        public int Quantity { get; set; }

        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }

        public DrinkViewModel()
        {
        }

        public DrinkViewModel(int id, string name, int price, int quantity, byte[] imageData, string imageMimeType)
        {
            DrinkId = id;
            Name = name;
            Price = price;
            Quantity = quantity;
            ImageData = imageData;
            ImageMimeType = imageMimeType;
        }
    }
}