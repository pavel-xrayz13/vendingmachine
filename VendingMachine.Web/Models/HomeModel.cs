﻿using System.Collections.Generic;

namespace VendingMachine.Web.Models
{
    public class HomeModel
    {
        public IEnumerable<DrinkViewModel> Drinks { get; set; }
        public IEnumerable<CoinViewModel> Coins { get; set; }
    }
}