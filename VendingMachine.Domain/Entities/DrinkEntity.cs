﻿namespace VendingMachine.Domain.Entities
{
    public class DrinkEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public byte[] PreviewImage { get; set; }
    }
}
