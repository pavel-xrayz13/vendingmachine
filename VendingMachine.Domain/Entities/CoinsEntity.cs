﻿namespace VendingMachine.Domain.Entities
{
    public class CoinsEntity
    {
        public int Id { get; set; }
        public int Nominal { get; set; }
        public int Quantity { get; set; }
    }
}