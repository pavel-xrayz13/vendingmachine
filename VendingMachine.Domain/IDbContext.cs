﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace VendingMachine.Domain
{
    public interface IDbContext : IDisposable
    {
        void AddEntity<TEntity>(TEntity entity) where TEntity : class;
        void UpdateEntity<TEntity>(TEntity entity) where TEntity : class;
        void RemoveEntity<TEntity>(TEntity entity) where TEntity : class;
        TEntity FindEntity<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class;
        IQueryable<TEntity> Query<TEntity>(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null) where TEntity : class;

        void Commit();
    }
}
