﻿namespace VendingMachine.Domain
{
    public interface IDbContextFactory
    {
        IDbContext Get(bool commitOnDispose = true);
    }
}
